package org.taruts.djig.core.controller.refresh.hub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.taruts.djig.core.webhook.hub.GitHubPushEventCallbackService;

import java.util.Set;

@RestController
@RequestMapping("#{@djigConfigurationProperties.controller.refresh.path}/github")
public class GitHubPushEventCallbackController {

    private static final String REF_PREFIX = "refs/heads/";

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private GitHubPushEventCallbackService gitHubPushEventCallbackService;

    @PostMapping(headers = "X-GitHub-Event=push")
    public void postPush(@RequestBody GitHubPushEventCallbackDto dto) {
        refreshProjectsAsync(dto);
    }

    @PostMapping(headers = "X-GitHub-Event=ping")
    public void postPing() {
    }

    private void refreshProjectsAsync(GitHubPushEventCallbackDto dto) {
        taskExecutor.execute(() -> refreshProjects(dto));
    }

    private void refreshProjects(GitHubPushEventCallbackDto dto) {
        Set<String> urls = getUrls(dto);
        String branch = getBranch(dto);
        gitHubPushEventCallbackService.refreshProjects(urls, branch);
    }

    private static Set<String> getUrls(GitHubPushEventCallbackDto dto) {
        GitHubRepositoryDto repository = dto.getRepository();
        return Set.of(
                repository.getCloneUrl(),
                repository.getSshUrl(),
                repository.getGitUrl()
        );
    }

    private static String getBranch(GitHubPushEventCallbackDto dto) {
        // This is a string like "refs/heads/master"
        String ref = dto.getRef();
        if (!ref.startsWith(REF_PREFIX)) {
            throw new IllegalArgumentException("ref is expected to start with " + REF_PREFIX);
        }
        return ref.substring(REF_PREFIX.length());
    }
}
