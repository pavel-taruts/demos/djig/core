package org.taruts.djig.core.controller.refresh.hub;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GitHubPushEventCallbackDto {
    String ref;
    GitHubRepositoryDto repository;
}
