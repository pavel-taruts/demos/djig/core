package org.taruts.djig.core.controller.refresh.hub;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class GitHubRepositoryDto {
    Long id;
    String name;
    String cloneUrl;
    String sshUrl;
    String gitUrl;
}
