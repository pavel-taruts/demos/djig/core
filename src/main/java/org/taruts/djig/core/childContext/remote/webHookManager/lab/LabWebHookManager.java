package org.taruts.djig.core.childContext.remote.webHookManager.lab;

import lombok.SneakyThrows;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.ProjectHook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.configurationProperties.HostingType;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectRepository;
import org.taruts.djig.core.childContext.remote.DynamicProjectGitRemote;
import org.taruts.djig.core.childContext.remote.webHookManager.WebHookManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class LabWebHookManager implements WebHookManager {

    @Autowired
    private DynamicProjectRepository dynamicProjectRepository;

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    @Override
    public void replaceHooks(URI baseHookUri) {
        getGitLabDynamicProjects().forEach(dynamicProject ->
                replaceHook(dynamicProject, baseHookUri)
        );
    }

    @Override
    public void deleteHooks(URI baseHookUri) {
        getGitLabDynamicProjects().forEach(dynamicProject ->
                deleteHooks(dynamicProject, baseHookUri)
        );
    }

    private void deleteHooks(DynamicProject dynamicProject, URI baseHookUri) {
        withGitLabProject(
                dynamicProject,
                (gitLabApi, gitLabProject) -> deleteHooksByUri(dynamicProject, baseHookUri, gitLabApi, gitLabProject)
        );
    }

    private Stream<DynamicProject> getGitLabDynamicProjects() {
        return dynamicProjectRepository.byHostingType(HostingType.GitLab);
    }

    private void replaceHook(DynamicProject dynamicProject, URI baseHookUri) {
        withGitLabProject(dynamicProject, (gitLabApi, gitLabProject) -> {
            deleteHooksByUri(dynamicProject, baseHookUri, gitLabApi, gitLabProject);
            addHook(dynamicProject, baseHookUri, gitLabApi, gitLabProject);
        });
    }

    @SneakyThrows
    private void withGitLabProject(DynamicProject dynamicProject, BiConsumer<GitLabApi, Project> useProject) {
        DynamicProjectGitRemote remoteProperties = dynamicProject.getRemote();
        String repositoryUrlStr = remoteProperties.url();
        String username = remoteProperties.username();
        String password = remoteProperties.password();

        URI repositoryUri = URI.create(repositoryUrlStr);

        String projectPath = repositoryUri.getPath().replaceAll("^/(.*)\\.git$", "$1");

        UriComponents gitlabUriComponents = UriComponentsBuilder
                .fromUri(repositoryUri)
                .replacePath(null)
                .build();
        String gitlabUrlStr = gitlabUriComponents.toString();

        try (GitLabApi gitLabApi = GitLabApiFactory.getGitLabApi(gitlabUrlStr, username, password)) {
            Project project = gitLabApi
                    .getProjectApi()
                    .getOptionalProject(projectPath)
                    .orElseThrow(() -> new IllegalStateException("Project not found by path %s".formatted(projectPath)));
            useProject.accept(gitLabApi, project);
        }
    }

    private void deleteHooksByUri(DynamicProject dynamicProject, URI baseHookUri, GitLabApi gitLabApi, Project project) {
        URI hookUri = getHookUri(dynamicProject, baseHookUri);
        List<ProjectHook> hooksToDelete = findHooksToDeleteByUri(gitLabApi, project, hookUri);
        hooksToDelete.forEach(hookToDelete -> {
            try {
                gitLabApi.getProjectApi().deleteHook(hookToDelete);
            } catch (GitLabApiException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @SneakyThrows
    private List<ProjectHook> findHooksToDeleteByUri(GitLabApi gitLabApi, Project project, URI hookUri) {
        List<ProjectHook> allProjectHooks = gitLabApi.getProjectApi().getHooks(project.getId());
        return allProjectHooks.stream().filter(currentHook -> {
            try {
                URI currentHookUri = new URI(currentHook.getUrl());
                return URI_COMPARATOR.compare(currentHookUri, hookUri) == 0;
            } catch (URISyntaxException e) {
                // All bad URIs must be deleted
                return true;
            }
        }).toList();
    }

    private static URI getHookUri(DynamicProject dynamicProject, URI baseHookUri) {
        return UriComponentsBuilder
                .fromUri(baseHookUri)
                .pathSegment("generic", dynamicProject.getName())
                .build()
                .toUri();
    }

    private void addHook(DynamicProject dynamicProject, URI baseHookUri, GitLabApi gitLabApi, Project gitLabProject) {
        URI hookUri = getHookUri(dynamicProject, baseHookUri);
        String branch = dynamicProject.getRemote().branch();

        DjigConfigurationProperties.Hook hookProperties = djigConfigurationProperties.getHook();

        boolean enableSslVerification = hookProperties.isSslVerification();
        String secretToken = hookProperties.getSecret();

        ProjectHook enabledHooks = new ProjectHook();

        //@formatter:off
        enabledHooks.setPushEvents               (true);
        enabledHooks.setPushEventsBranchFilter   (branch);
        enabledHooks.setIssuesEvents             (false);
        enabledHooks.setConfidentialIssuesEvents (false);
        enabledHooks.setMergeRequestsEvents      (false);
        enabledHooks.setTagPushEvents            (false);
        enabledHooks.setNoteEvents               (false);
        enabledHooks.setConfidentialNoteEvents   (false);
        enabledHooks.setJobEvents                (false);
        enabledHooks.setPipelineEvents           (false);
        enabledHooks.setWikiPageEvents           (false);
        enabledHooks.setRepositoryUpdateEvents   (false);
        enabledHooks.setDeploymentEvents         (false);
        enabledHooks.setReleasesEvents           (false);
        enabledHooks.setDeploymentEvents         (false);
        //@formatter:on

        try {
            gitLabApi.getProjectApi().addHook(gitLabProject.getId(), hookUri.toString(), enabledHooks, enableSslVerification, secretToken);
        } catch (GitLabApiException e) {
            throw new RuntimeException(e);
        }
    }
}
