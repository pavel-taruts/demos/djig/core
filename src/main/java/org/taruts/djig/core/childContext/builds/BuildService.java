package org.taruts.djig.core.childContext.builds;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProject;
import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProjectProvider;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;

import java.io.File;
import java.util.List;

public class BuildService {

    @Autowired
    BuildableDynamicProjectProvider buildableDynamicProjectProvider;

    public DynamicProjectBuild build(DynamicProject dynamicProject) {
        BuildableDynamicProject buildableDynamicProject = buildableDynamicProjectProvider.getBuildable(dynamicProject);
        buildInternal(buildableDynamicProject);
        List<File> dynamicDependencies = getDynamicDependencies(buildableDynamicProject);
        return composeResult(buildableDynamicProject, dynamicDependencies);
    }

    private void buildInternal(BuildableDynamicProject buildableDynamicProject) {
        List<String> parameters = buildableDynamicProject.buildSystemService().getParameters();
        buildableDynamicProject.runBuildScript(parameters);
    }

    private static List<File> getDynamicDependencies(BuildableDynamicProject buildableDynamicProject) {
        List<File> dynamicDependencies = buildableDynamicProject
                .buildSystemService()
                .getDynamicDependenciesProvider()
                .get(buildableDynamicProject);
        return dynamicDependencies;
    }

    private DynamicProjectBuild composeResult(BuildableDynamicProject buildableDynamicProject, List<File> dynamicDependencies) {
        File sourceDirectory = buildableDynamicProject.sourceDirectory();
        BuildSystemService buildSystemService = buildableDynamicProject.buildSystemService();

        List<String> classesDirectoriesStr = buildSystemService.getClassesDirectories();
        List<File> classesDirectories = getClassesDirectories(sourceDirectory, classesDirectoriesStr);

        List<String> resourceDirectoriesStr = buildSystemService.getResourceDirectories();
        List<File> resourceDirectories = getResourceDirectories(sourceDirectory, resourceDirectoriesStr);

        return new DynamicProjectBuild(classesDirectories, resourceDirectories, dynamicDependencies);
    }

    private List<File> getClassesDirectories(File projectSourceDirectory, List<String> classesDirectoriesStr) {
        List<File> classesDirectories = classesDirectoriesStr
                .stream()
                .map(classesDirectoryStr -> FileUtils.getFile(projectSourceDirectory, classesDirectoryStr))
                .filter(file -> file.exists() && file.isDirectory())
                .toList();
        if (classesDirectories.isEmpty()) {
            throw new IllegalStateException("It seems the build produced no classes");
        }
        return classesDirectories;
    }

    private List<File> getResourceDirectories(File projectSourceDirectory, List<String> resourceDirectoriesStr) {
        return resourceDirectoriesStr
                .stream()
                .map(resourceDirectoryStr -> FileUtils.getFile(projectSourceDirectory, resourceDirectoryStr))
                .filter(file -> file.exists() && file.isDirectory())
                .toList();
    }
}
