package org.taruts.djig.core.childContext.remote;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.core.utils.DjigStringUtils;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.function.Consumer;

public class BaseHookUriProvider {

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    @Autowired
    private WebServerApplicationContext webServerApplicationContext;

    @Getter
    private URI baseHookUri;

    @PostConstruct
    public void init() {
        baseHookUri = get();
    }

    @SneakyThrows
    private URI get() {
        DjigConfigurationProperties.Hook hookProperties = djigConfigurationProperties.getHook();

        String host = hookProperties.getHost();
        if (StringUtils.isBlank(host)) {
            return null;
        }

        String protocol = hookProperties.getProtocol();

        int port = hookProperties.getPort();
        if (port == -1) {
            port = webServerApplicationContext.getWebServer().getPort();
        }

        String refreshPath = djigConfigurationProperties.getController().getRefresh().getPath();
        refreshPath = DjigStringUtils.ensureEndsWithSlash(refreshPath);
        return UriComponentsBuilder
                .newInstance()
                .scheme(protocol)
                .host(host)
                .port(port)
                .replacePath(refreshPath)
                .build()
                .toUri();
    }

    public void withBaseHookUri(Consumer<URI> consumer) {
        if (baseHookUri != null) {
            consumer.accept(baseHookUri);
        }
    }
}
