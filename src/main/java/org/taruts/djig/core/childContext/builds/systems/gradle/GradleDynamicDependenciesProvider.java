package org.taruts.djig.core.childContext.builds.systems.gradle;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProject;
import org.taruts.djig.core.childContext.builds.systems.base.DynamicDependenciesProvider;
import org.taruts.processUtils.ProcessRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class GradleDynamicDependenciesProvider implements DynamicDependenciesProvider {

    @Override
    @SneakyThrows
    public List<File> get(BuildableDynamicProject buildableDynamicProject) {
        File sourceDirectory = buildableDynamicProject.sourceDirectory();
        File fileToTweak = null;
        try {
            Language language = detectLanguage(sourceDirectory);
            fileToTweak = FileUtils.getFile(sourceDirectory, language.fileName);
            tweak(fileToTweak, language);
            return doGet(buildableDynamicProject);
        } finally {
            rollbackTweak(sourceDirectory, fileToTweak);
        }
    }

    private static void rollbackTweak(File sourceDirectory, File tweakedFile) throws IOException {
        if (tweakedFile != null) {
            ProcessRunner.runProcess(sourceDirectory, "git", "checkout", "--", tweakedFile.getCanonicalPath());
        }
    }

    private static Language detectLanguage(File sourceDirectory) {
        List<Language> languages = Arrays
                .stream(Language.values())
                .filter(language -> FileUtils.getFile(sourceDirectory, language.fileName).exists())
                .toList();

        if (languages.isEmpty()) {
            throw new IllegalStateException("No Gradle build scripts found in %s. There must be just one".formatted(sourceDirectory));
        }

        if (languages.size() > 1) {
            throw new IllegalStateException(
                    "%d Gradle build scripts found in %s. There must be just one".formatted(languages.size(), sourceDirectory));
        }

        Language language = languages.get(0);
        return language;
    }

    private static void tweak(File file, Language language) throws IOException {
        try (
                FileWriter fileWriter = new FileWriter(file, true);
                BufferedWriter writer = new BufferedWriter(fileWriter)
        ) {
            writer.append("\n").append(language.tweak);
        }
    }

    private static List<File> doGet(BuildableDynamicProject buildableDynamicProject) {
        String output = buildableDynamicProject.runBuildScript(
                "--console", "plain",
                "--quiet",
                "printClasspath"
        );

        return Arrays
                .stream(output.split(";"))
                .map(File::new)
                .toList();
    }

    enum Language {
        Groovy(
                "build.gradle",
                """
                        tasks.register("printClasspath") {
                            doFirst {
                                println(configurations.runtimeClasspath.get().asPath)
                            }
                        }
                        """
        ),
        Kotlin(
                "build.gradle.kts",
                """
                        tasks.register("printClasspath") {
                            doFirst {
                                println(configurations.runtimeClasspath.get().asPath)
                            }
                        }
                        """
        );

        final String fileName;
        final String tweak;

        Language(String fileName, String tweak) {
            this.fileName = fileName;
            this.tweak = tweak;
        }
    }
}
