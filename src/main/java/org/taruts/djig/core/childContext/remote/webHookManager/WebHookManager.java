package org.taruts.djig.core.childContext.remote.webHookManager;

import java.net.URI;
import java.util.Comparator;

public interface WebHookManager {

    Comparator<URI> URI_COMPARATOR = Comparator
            .comparing(URI::getHost)
            .thenComparing(URI::getPort)
            .thenComparing(URI::getPath);

    void replaceHooks(URI baseHookUri);

    void deleteHooks(URI baseHookUri);
}
