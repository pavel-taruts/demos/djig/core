package org.taruts.djig.core.childContext.builds.buildableDynamicProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.BuildType;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.childContext.builds.systems.BuildSystemServiceSelector;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;

import java.io.File;

public class BuildableDynamicProjectProvider {

    @Autowired
    BuildableDynamicProjectAutoDetector buildableDynamicProjectAutoDetector;

    @Autowired
    BuildSystemServiceSelector buildSystemServiceSelector;

    public BuildableDynamicProject getBuildable(DynamicProject dynamicProject) {
        File sourceDirectory = dynamicProject.getSourceDirectory();
        BuildType buildType = dynamicProject.getBuildType();

        if (buildType == null) {
            BuildableDynamicProject detectResult = buildableDynamicProjectAutoDetector.detect(sourceDirectory);
            if (detectResult == null) {
                throw new NullPointerException("Failed to detect the build type for project %s".formatted(sourceDirectory));
            }
            return detectResult;
        } else {
            BuildSystemService buildSystemService = buildSystemServiceSelector.select(buildType);

            if (buildType.isWrapper()) {
                String wrapperShellScriptFileName = buildableDynamicProjectAutoDetector.findWrapperScriptFileNameWithExtension(sourceDirectory, buildSystemService);
                if (wrapperShellScriptFileName == null) {
                    throw new NullPointerException("There is no wrapper shell script %s in %s".formatted(
                            buildSystemService.getWrapperScriptName(), sourceDirectory
                    ));
                }
                return new BuildableDynamicProject(sourceDirectory, wrapperShellScriptFileName, buildSystemService);
            } else {
                return new BuildableDynamicProject(sourceDirectory, null, buildSystemService);
            }
        }
    }
}
