package org.taruts.djig.core.childContext.remote;

import org.taruts.djig.configurationProperties.HostingType;

public record DynamicProjectGitRemote(HostingType hostingType, String url, String username, String password, String branch) {
}
