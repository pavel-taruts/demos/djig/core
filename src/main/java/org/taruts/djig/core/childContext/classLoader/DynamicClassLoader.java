package org.taruts.djig.core.childContext.classLoader;

import lombok.Getter;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

public class DynamicClassLoader extends URLClassLoader {

    @Getter
    final List<File> classesDirectories;

    public DynamicClassLoader(File sourceDirectory, List<File> classesDirectories, List<File> resourcesDirectories, List<File> dynamicDependencies) {
        super(
                sourceDirectory.getName(),
                filesToUrls(classesDirectories, resourcesDirectories, dynamicDependencies),
                DynamicClassLoader.class.getClassLoader()
        );
        this.classesDirectories = classesDirectories;
    }

    private static URL[] filesToUrls(List<File> classesDirectories, List<File> resourcesDirectories, List<File> dynamicDependencies) {
        return Stream
                .of(
                        classesDirectories.stream(),
                        resourcesDirectories.stream(),
                        dynamicDependencies.stream()
                )
                .flatMap(Function.identity())
                .filter(Objects::nonNull)
                .map(DynamicClassLoader::fileToUrl)
                .toArray(URL[]::new);
    }

    private static URL fileToUrl(File directory) {
        try {
            return UriComponentsBuilder
                    .fromUri(directory.toURI())
                    .scheme("file")
                    .build()
                    .toUri()
                    .toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * First we look for a resource among those of this class loader
     * and only after that we delegate to the parent.
     * The original implementation {@link ClassLoader#getResource(String)} works in the opposite way.
     * We changed the order this way to enable the dynamic code to override properties in application*.properties files
     * of the main Spring context (of the main class loader).
     */
    @Override
    public URL getResource(String name) {
        Objects.requireNonNull(name);

        // First we try to find the resource among those belonging directly to the class loader
        URL url = findResource(name);

        // Then we delegate to the parent
        if (url == null) {
            ClassLoader parent = getParent();
            if (parent != null) {
                url = parent.getResource(name);
            }
        }

        return url;
    }
}
