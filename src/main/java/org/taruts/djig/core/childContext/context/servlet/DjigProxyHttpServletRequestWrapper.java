package org.taruts.djig.core.childContext.context.servlet;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class DjigProxyHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private final String contextPath;
    private final String servletPath;

    public DjigProxyHttpServletRequestWrapper(HttpServletRequest request, String contextPath) {
        super(request);

        String requestPath = getRequestPath();
        if (!requestPath.startsWith(contextPath)) {
            throw new IllegalArgumentException("requestPath = %s, contextPath = %s. requestPath must start with contextPath");
        }
        this.contextPath = contextPath;

        this.servletPath = requestPath.substring(contextPath.length());
    }

    private String getRequestPath() {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(getRequestURL().toString())
                .build();
        return uriComponents.getPath();
    }

    @Override
    public String getContextPath() {
        return contextPath;
    }

    @Override
    public String getServletPath() {
        return servletPath;
    }
}
