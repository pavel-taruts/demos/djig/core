package org.taruts.djig.core.childContext.builds.systems.maven;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.BuildSystem;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;

import java.util.List;

public class MavenService implements BuildSystemService {

    @Autowired
    @Getter
    MavenDynamicDependenciesProvider dynamicDependenciesProvider;

    @Override
    public BuildSystem getBuildSystem() {
        return BuildSystem.MAVEN;
    }

    @Override
    public List<String> getFileNames() {
        return List.of("pom.xml");
    }

    @Override
    public String getExecutableName() {
        return "mvn";
    }

    @Override
    public String getWrapperScriptName() {
        return "mvnw";
    }

    @Override
    public List<String> getParameters() {
        return List.of("compile");
    }

    @Override
    public List<String> getClassesDirectories() {
        return List.of("target/classes");
    }

    @Override
    public List<String> getResourceDirectories() {
        return List.of();
    }
}
