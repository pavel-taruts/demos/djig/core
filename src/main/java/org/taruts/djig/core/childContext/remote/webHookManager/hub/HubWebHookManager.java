package org.taruts.djig.core.childContext.remote.webHookManager.hub;

import lombok.SneakyThrows;
import org.kohsuke.github.GHEvent;
import org.kohsuke.github.GHHook;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.configurationProperties.HostingType;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectRepository;
import org.taruts.djig.core.childContext.remote.DynamicProjectGitRemote;
import org.taruts.djig.core.childContext.remote.webHookManager.WebHookManager;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HubWebHookManager implements WebHookManager {

    @SuppressWarnings("HttpUrlsUsage")
    private static final List<String> URL_PREFIXES = List.of(
            "git://github.com/",
            "git@github.com:",
            "https://github.com/",
            "http://github.com/",
            "github.com/"
    );

    private static final String URL_PREFIXES_STR = String.join(", ", URL_PREFIXES);

    @Autowired
    private DynamicProjectRepository dynamicProjectRepository;

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    @Override
    public void replaceHooks(URI baseHookUri) {
        URI hookUri = getHookUri(baseHookUri);
        withDistinctGhRepositories((ghRepository, dynamicProject) -> {
            deleteHook(ghRepository, hookUri);
            addHook(ghRepository, hookUri);
        });
    }

    @Override
    public void deleteHooks(URI baseHookUri) {
        URI hookUri = getHookUri(baseHookUri);
        withDistinctGhRepositories((ghRepository, dynamicProject) ->
                deleteHook(ghRepository, hookUri)
        );
    }

    @SneakyThrows
    private void deleteHook(GHRepository ghRepository, URI hookUri) {
        ghRepository
                .getHooks()
                .stream()
                .filter(hook -> {
                    URI uri = extractHookUriFromHook(hook);
                    return URI_COMPARATOR.compare(uri, hookUri) == 0;
                })
                .forEach(hook -> {
                    try {
                        hook.delete();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    @SneakyThrows
    private void addHook(GHRepository ghRepository, URI hookUri) {

        DjigConfigurationProperties.Hook hookProperties = djigConfigurationProperties.getHook();

        boolean insecureSsl = !hookProperties.isSslVerification();

        ghRepository.createHook(
                "web",
                Map.of(
                        "url", hookUri.toString(),
                        "content_type", "json",
                        "insecure_ssl", insecureSsl ? "1" : "0",
                        "secret", hookProperties.getSecret()
                ),
                List.of(GHEvent.PUSH),
                true
        );
    }

    private void withDistinctGhRepositories(BiConsumer<GHRepository, DynamicProject> consumer) {

        Map<String, List<DynamicProject>> pathToProjectsMap = getPathToProjectsMap();

        pathToProjectsMap.forEach((projectPath, dynamicProjects) -> {
            DynamicProject dynamicProject = dynamicProjects.get(0);
            GHRepository repository = connectToRepository(projectPath, dynamicProject);
            consumer.accept(repository, dynamicProject);
        });
    }

    private Map<String, List<DynamicProject>> getPathToProjectsMap() {
        Map<String, List<DynamicProject>> pathToProjectsMap = getGitHubDynamicProjects().collect(Collectors.groupingBy(
                dynamicProject -> getProjectPath(dynamicProject.getRemote().url())
        ));
        return pathToProjectsMap;
    }

    private Stream<DynamicProject> getGitHubDynamicProjects() {
        return dynamicProjectRepository.byHostingType(HostingType.GitHub);
    }

    @SneakyThrows
    private static URI extractHookUriFromHook(GHHook hook) {
        Map<String, String> config = hook.getConfig();
        String url = config.get("url");
        URI uri = new URI(url);
        return uri;
    }

    private static URI getHookUri(URI baseHookUri) {
        return UriComponentsBuilder
                .fromUri(baseHookUri)
                .pathSegment("github")
                .build()
                .toUri();
    }

    private static String getProjectPath(String url) {
        String urlPath = URL_PREFIXES
                .stream()
                .filter(url::startsWith)
                .map(String::length)
                .map(url::substring)
                .findAny()
                .orElseThrow(() ->
                        new IllegalStateException(
                                "URL %s doesn't use any of the allowed prefixes: %s"
                                        .formatted(url, URL_PREFIXES_STR)
                        )
                );

        if (!urlPath.endsWith(".git")) {
            throw new IllegalStateException("URL %s doesn't end with .git");
        }
        String projectPath = urlPath.substring(0, urlPath.length() - ".git".length());
        return projectPath;
    }

    @SneakyThrows
    private static GHRepository connectToRepository(String projectPath, DynamicProject dynamicProject) {
        return connect(dynamicProject).getRepository(projectPath);
    }

    private static GitHub connect(DynamicProject dynamicProject) {
        DynamicProjectGitRemote remote = dynamicProject.getRemote();
        return GitHubFactory.getGitHub(remote.username(), remote.password());
    }
}
