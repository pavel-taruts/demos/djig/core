package org.taruts.djig.core.childContext.remote;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.taruts.djig.core.OurSmartLifecycle;
import org.taruts.djig.core.childContext.remote.webHookManager.WebHookManager;

import java.util.List;

/**
 * For any dynamic project registers a webhook in the dynamic code GitLab project, after the application has started
 * and removes the hook on shutdown.
 * The webhook notifies the application when a new version of the dynamic code has been pushed.
 */
@Slf4j
public class WebHookRegistrar extends OurSmartLifecycle implements Ordered {

    @Autowired
    private BaseHookUriProvider baseHookUriProvider;

    @Autowired
    private List<WebHookManager> webHookManagers;

    @Override
    public int getOrder() {
        return -1;
    }

    @Override
    public void doStop() {
        baseHookUriProvider.withBaseHookUri(baseHookUri ->
                webHookManagers.forEach(webHookManager -> webHookManager.deleteHooks(baseHookUri))
        );
    }

    @Override
    public void doStart() {
        baseHookUriProvider.withBaseHookUri(baseHookUri ->
                webHookManagers.forEach(webHookManager -> webHookManager.replaceHooks(baseHookUri))
        );
    }
}
