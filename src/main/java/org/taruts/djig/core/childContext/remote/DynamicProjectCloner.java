package org.taruts.djig.core.childContext.remote;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.gitUtils.GitUtils;
import org.taruts.gitUtils.hosting.credentials.CredentialsUtils;
import org.taruts.processUtils.ProcessRunner;

import java.io.File;
import java.nio.file.Path;
import java.util.stream.Stream;

@Slf4j
public class DynamicProjectCloner {

    @Autowired
    private CloneRetryTemplate cloneRetryTemplate;

    public void cloneWithRetries(DynamicProjectGitRemote remote, File projectSourceDirectory) {
        String url = tweakRemoteUrlIfLocalDirectory(remote.url());
        cloneRetryTemplate.execute(retryContext -> {
            cloneOrUpdate(url, remote.username(), remote.password(), remote.branch(), projectSourceDirectory);
            return null;
        });
    }

    private String tweakRemoteUrlIfLocalDirectory(String urlArg) {
        if (isRemoteLocalDirectory(urlArg)) {
            // Add "file://" so that cloning would be done in a similar way as with remotes on the network.
            // Plus, transform the path from relative to absolute.
            return "file://" + Path.of(urlArg).toAbsolutePath().normalize();
        } else {
            return urlArg;
        }
    }

    private boolean isRemoteLocalDirectory(String urlArg) {
        return Stream.of(
                "http:", "https:", "file:", "git@"
        ).noneMatch(urlArg::startsWith);
    }

    @SneakyThrows
    public static void cloneOrUpdate(String url, String username, String password, String branch, File directory) {
        String[] gitUtilityCredentials = CredentialsUtils.apiCredentialsToGitUtilityCredentials(username, password);
        username = gitUtilityCredentials[0];
        password = gitUtilityCredentials[1];

        directory = new File(directory.getCanonicalPath());
        if (directory.exists()) {
            try {
                updateWorkingTree(url, username, password, branch, directory);
            } catch (Exception e) {
                log.error("Could not update git repository", e);
                FileUtils.deleteDirectory(directory);
            }
        }
        if (!directory.exists()) {
            GitUtils.clone(url, username, password, branch, directory);
            removeRemote(directory);
        }
    }

    private static void updateWorkingTree(String url, String username, String password, String branch, File directory) {
        addRemote(url, username, password, branch, directory);
        try {
            ProcessRunner.runProcess(directory, "git", "reset", "--hard", "origin/" + branch);
        } finally {
            // We do not leave credentials in .git/config/[remote "origin"]/url for long
            removeRemote(directory);
        }
    }

    private static void addRemote(String url, String username, String password, String branch, File directory) {
        if (StringUtils.isNotBlank(username)) {
            url = GitUtils.addCredentialsToGitRepositoryUrl(url, username, password);
        }
        ProcessRunner.runProcess(
                directory,
                "git", "remote", "add",
                // origin/<branch> will be the only tracking branch
                "-t", branch,
                // Fetch right after adding the remote
                "-f",
                // The remote name is "origin"
                "origin",
                // The remote URL
                url
        );
        // Note that we do not set origin/<branch> as the upstream for our local branch,
        // because we don't do pull, push, etc.
        // We just use origin/<branch> in reset --hard.
        // It doesn't require origin/<branch> to be the upstream for the branch being reset.
    }

    private static void removeRemote(File directory) {
        ProcessRunner.runProcess(directory, "git", "remote", "remove", "origin");
    }
}
