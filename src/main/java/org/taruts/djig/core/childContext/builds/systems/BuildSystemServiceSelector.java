package org.taruts.djig.core.childContext.builds.systems;

import org.taruts.djig.configurationProperties.BuildSystem;
import org.taruts.djig.configurationProperties.BuildType;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;
import org.taruts.djig.core.selector.AbstractEnumKeySelector;

public class BuildSystemServiceSelector extends AbstractEnumKeySelector<BuildSystem, BuildSystemService> {

    protected BuildSystemServiceSelector() {
        super(BuildSystemService::getBuildSystem, BuildSystem.class);
    }

    public BuildSystemService select(BuildType buildType) {
        return select(buildType.getBuildSystem());
    }
}
