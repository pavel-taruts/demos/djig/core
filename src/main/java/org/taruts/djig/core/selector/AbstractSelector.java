package org.taruts.djig.core.selector;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class AbstractSelector<K, T> {

    private final Function<T, K> getKey;
    private final Supplier<Map<K, T>> mapFactory;
    @Autowired
    protected List<T> list;
    private Map<K, T> map;

    protected AbstractSelector(Function<T, K> getKey) {
        this.getKey = getKey;
        this.mapFactory = HashMap::new;
    }

    protected AbstractSelector(Function<T, K> getKey, Supplier<Map<K, T>> mapFactory) {
        this.getKey = getKey;
        this.mapFactory = mapFactory;
    }

    @PostConstruct
    public void init() {
        map = list
                .stream()
                .collect(
                        Collectors.toMap(
                                getKey,
                                x -> x,
                                (a, b) -> {
                                    K key = getKey.apply(a);
                                    throw new IllegalStateException(
                                            "There are two objects for key %s: %s and %s".formatted(key, a, b));
                                },
                                mapFactory
                        )
                );
    }

    public T select(K key) {
        return map.get(key);
    }
}
