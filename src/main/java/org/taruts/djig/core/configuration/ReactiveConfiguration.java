package org.taruts.djig.core.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.reactive.DispatcherHandler;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;
import org.taruts.djig.core.childContext.context.DynamicApplicationContext;
import org.taruts.djig.core.childContext.context.reactive.DjigProxyHandlerMapping;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(DispatcherHandler.class)
public class ReactiveConfiguration {

    @Bean
    DjigProxyHandlerMapping djigProxyHandlerMapping() {
        return new DjigProxyHandlerMapping(Ordered.HIGHEST_PRECEDENCE);
    }

    @Bean
    DynamicApplicationContextCustomizer dynamicApplicationContextCustomizer() {
        return (DynamicApplicationContext dynamicApplicationContext) -> {
            dynamicApplicationContext.registerBean(RequestMappingHandlerMapping.class);
        };
    }
}
