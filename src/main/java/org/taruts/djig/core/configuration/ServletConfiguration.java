package org.taruts.djig.core.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.taruts.djig.core.childContext.context.DynamicApplicationContext;
import org.taruts.djig.core.childContext.context.servlet.DjigProxyHandlerMapping;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(DispatcherServlet.class)
public class ServletConfiguration {

    @Bean
    DjigProxyHandlerMapping djigProxyHandlerMapping() {
        return new DjigProxyHandlerMapping(Ordered.HIGHEST_PRECEDENCE);
    }

    @Bean
    DynamicApplicationContextCustomizer dynamicApplicationContextCustomizer() {
        return (DynamicApplicationContext dynamicApplicationContext) -> {
            dynamicApplicationContext.registerBean(RequestMappingHandlerMapping.class);
        };
    }
}
