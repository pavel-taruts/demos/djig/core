package org.taruts.djig.core.configurationProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectManager;

import javax.annotation.PostConstruct;

public class DynamicProjectLoaderFromConfigurationProperties {

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    @Autowired
    private DynamicProjectManager dynamicProjectManager;

    @Autowired
    private DynamicProjectConfigurationPropertiesMapper dynamicProjectConfigurationPropertiesMapper;

    @PostConstruct
    private void createAndLoadDynamicProjects() {
        djigConfigurationProperties.getDynamicProjects().forEach((dynamicProjectName, dynamicProjectConfigurationProperties) -> {
            DynamicProject dynamicProject = dynamicProjectConfigurationPropertiesMapper.map(dynamicProjectName, dynamicProjectConfigurationProperties);
            dynamicProjectManager.addProject(dynamicProject);
        });
    }
}
