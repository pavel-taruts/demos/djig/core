package org.taruts.djig.core.configurationProperties;

import com.google.common.base.Functions;
import lombok.RequiredArgsConstructor;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.configurationProperties.HostingType;
import org.taruts.djig.configurationProperties.utils.DynamicProjectPropertiesDefaultsApplier;
import org.taruts.djig.configurationProperties.utils.HostingTypeDetector;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.childContext.remote.DynamicProjectGitRemote;
import org.taruts.djig.core.childContext.source.DynamicProjectSourceLocator;

import java.io.File;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DynamicProjectConfigurationPropertiesMapper {

    private final DjigConfigurationProperties djigConfigurationProperties;

    public Map<String, DynamicProject> map(Map<String, DjigConfigurationProperties.DynamicProject> dynamicProjectsConfigurationProperties) {
        return dynamicProjectsConfigurationProperties
                .entrySet()
                .stream()
                .map(entry -> {
                    String dynamicProjectName = entry.getKey();
                    DjigConfigurationProperties.DynamicProject dynamicProjectConfigurationProperties = entry.getValue();
                    return map(dynamicProjectName, dynamicProjectConfigurationProperties);
                })
                .collect(Collectors.toMap(DynamicProject::getName, Functions.identity()));
    }

    public DynamicProject map(
            String dynamicProjectName,
            DjigConfigurationProperties.DynamicProject dynamicProjectConfigurationProperties
    ) {

        dynamicProjectConfigurationProperties = DynamicProjectPropertiesDefaultsApplier.applyDefaults(djigConfigurationProperties, dynamicProjectConfigurationProperties);

        HostingType hostingType = dynamicProjectConfigurationProperties.getHostingType();
        String url = dynamicProjectConfigurationProperties.getUrl();
        if (hostingType == null) {
            hostingType = HostingTypeDetector.detect(url);
        }

        DynamicProjectGitRemote remote = new DynamicProjectGitRemote(
                hostingType,
                url,
                dynamicProjectConfigurationProperties.getUsername(),
                dynamicProjectConfigurationProperties.getPassword(),
                dynamicProjectConfigurationProperties.getBranch()
        );

        File sourceDirectory = DynamicProjectSourceLocator.getSourceDirectory(dynamicProjectName);

        return new DynamicProject(
                dynamicProjectName,
                remote,
                sourceDirectory,
                dynamicProjectConfigurationProperties.getDynamicInterfacePackage(),
                dynamicProjectConfigurationProperties.getBuildType()
        );
    }
}
